# README #

Headless CMS created with Wagtail using tutorials from LearnWagtail.com


### How do I get set up? ###
For the backend server
    ```python manage.py runserver```

To preview: 
- navigate to client folder
    ```python -m http.server 8020```
