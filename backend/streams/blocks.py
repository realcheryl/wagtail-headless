from wagtail.core import blocks
from wagtail.images.blocks import ImageChooserBlock
from wagtail.images.api.fields import ImageRenditionField

class TitleAndTextBlock(blocks.StructBlock):
    '''Title and text and nothing else '''
    title = blocks.CharBlock(required=True, help_text="Add your title")
    text = blocks.TextBlock(required=True, help_text = "Add additional text")

    class Meta: #noqa
        template = "streams/title_and_text_block.html"
        icon = "edit"
        label = "Title & Text"

class APIImageChooserBlock(ImageChooserBlock):
    def get_api_representation(self, value, context=None):
        img = ImageRenditionField('fill-200x200').to_representation(value)
        return {'image': img}
    
    class Meta: #noqa
        icon='picture'
        label='API Image'